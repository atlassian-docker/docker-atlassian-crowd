![Atlassian Crowd](https://wac-cdn.atlassian.com/dam/jcr:d2a1da52-ae52-4b06-9ab1-da8647a89653/crowd-icon-gradient-blue.svg?cdnVersion=696)

Crowd provides single sign-on and user identity that's easy to use.

Learn more about Crowd: [https://www.atlassian.com/software/crowd][1]

# Overview

This Docker container makes it easy to get an instance of Crowd up and running.

**Use docker version >= 20.10.10**

# Quick Start

For the `CROWD_HOME` directory that is used to store application data (amongst other things) we recommend mounting a host directory as a [data volume](https://docs.docker.com/engine/tutorials/dockervolumes/#/data-volumes), or via a named volume.

To get started you can use a data volume, or named volumes. In this example we'll use named volumes.

    docker volume create --name crowdVolume
    docker run -v crowdVolume:/var/atlassian/application-data/crowd --name="crowd" -d -p 8095:8095 atlassian/crowd


**Success**. Crowd is now available on [http://localhost:8095](http://localhost:8095).

Please ensure your container has the necessary resources allocated to it. See [Supported Platforms][2] for further information.


_* Note: If you are using `docker-machine` on Mac OS X, please use `open http://$(docker-machine ip default):8095` instead._

# Advanced Usage
For advanced usage, e.g. configuration, troubleshooting, supportability, etc.,
please check the [**Full Documentation**](https://atlassian.github.io/data-center-helm-charts/containers/CROWD/).


[1]: https://www.atlassian.com/software/crowd
[2]: https://confluence.atlassian.com/crowd/supported-platforms-191851.html
